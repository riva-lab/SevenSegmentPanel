/*
  SevenSegmentPanel.h
  Created by RIVA, 2019, 2022.

  Драйвер панели на 7-сегментных индикаторах.

  Индикаторы подключены к сдвиговым регистрам HC595 с использованием ULN2003
  (или напрямую, если потребляемый индикатором ток не превышает допустимого).
  Регистры соединяются последовательно.

  Панель управляется по 4 линиям:
  1 CLK     тактовый сигнал для регистров
  2 DATA    данные, загружаемые в регистры
  3 STR     сигнал разрешения обновления данных
  4 PWM     ШИМ-сигнал управления яркостью панели

  При указании выводов подключения панели для линии PWM следует использовать
  тот вывод, на котором доступен аппаратный ШИМ(~).

  Количество индикаторов нужно задать макросом PANEL_DIGITS, отредактировав
  его в файле "SevenSegmentPanel.h" (строка 46).
  По умолчанию - 8.

  Все функции, изменяющие отображаемые данные, работают во внутреннем буфере.
  Чтобы увидеть реальное изменение данных, необходимо использовать
  метод update() или autoUpdate().
*/

#ifndef SevenSegmentPanel_H
#define SevenSegmentPanel_H


#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include <avr/pgmspace.h>
#include <stdint.h>
#include "basic_font.h"
#include "basic_r_font.h"


#define PANEL_DIGITS                8   // кол-во индикаторов в панели
#define SSP_DEFAULT_BRIGHTNESS      255 // яркость панели по умолчанию
#define SSP_DEFAULT_FRAME_TIME      40  // длительность 1 кадра эффекта по умолчанию, мс
#define SSP_DEFAULT_BLINK_PERIOD    800 // период мигания цифр и/или курсора по умолчанию, мс


enum panelEffect_e {
  EFFECT_NONE,              // нет эффекта смены
  EFFECT_LIGHT_FADE,        // плавное затухание всей панели
  EFFECT_LIGHT_RISE,        // плавное зажигание всей панели
  EFFECT_LIGHT,             // плавное затухание/зажигание всей панели
  EFFECT_CENTER_IN,         // схлопывание к центру
  EFFECT_CENTER_OUT,        // развертывание от центра
  EFFECT_CENTER,            // комбинация схлопывание + развертывание
  EFFECT_FLOW_LEFT,         // волнообразное исчезновение влево
  EFFECT_FLOW_RIGHT,        // волнообразное исчезновение вправо
  EFFECT_WAVE_LEFT,         // волна влево
  EFFECT_WAVE_RIGHT,        // волна вправо
  EFFECT_SHIFT_LEFT,        // сдвиг текущих данных влево
  EFFECT_SHIFT_RIGHT,       // сдвиг текущих данных вправо
  EFFECT_MOVE_LEFT,         // передвижение текущих данных влево с заменой новыми
  EFFECT_MOVE_RIGHT,        // передвижение текущих данных вправо с заменой новыми
  EFFECT_SEG_HIDE,          // посегментное исчезновение
  EFFECT_SEG_SHOW,          // посегментное появление
  EFFECT_SEG_HIDESHOW,      // посегментное исчезновение/появление
  EFFECT_SPIN_CW,           // вращение меняющихся данных по ч.с.
  EFFECT_SPIN_CCW,          // вращение меняющихся данных против ч.с.
  EFFECT_SCROLL_DOWN,       // перелистывание меняющихся данных вниз
  EFFECT_SCROLL_UP          // перелистывание меняющихся данных вверх
};

typedef panelEffect_e panelEffect_t;


class SevenSegmentPanel {

  public:

    SevenSegmentPanel();
    SevenSegmentPanel(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm);
    ~SevenSegmentPanel();

    void begin(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm);
    void end();

    void    clear();                                                // очистка панели
    void    update();                                               // обновление информации на индикаторах (реальное)
    void    autoUpdate();                                           // обновление информации на индикаторах (реальное)
    void    startUpdating();                                        // начать обновление с применением заданного эффекта
    uint8_t isUpdating();                                           // флаг обновления: 1 - идет, 0 - завершено
    uint8_t digits();                                               // количество знакомест (цифр) панели

    void setDirection(uint8_t direction);                           // направление вывода цифр на панели
    void setDigitRotation(uint8_t position, uint8_t isRotate);      // установка поворота цифры
    void setBrightness(uint8_t brightness);                         // установка яркости панели (0 - выкл, 255 - максимальная)
    void setEffect(panelEffect_t effect_type = EFFECT_NONE);        // установка эффекта для обновлений панели
    void setEffectNext(panelEffect_t effect_type = EFFECT_NONE);    // установка эффекта для следующих обновлений панели
    void setFrameTime(uint8_t milliseconds);                        // установка длительности 1 кадра эффекта (миллисекунды)

    void cursorShow(uint8_t isShow);                                // задание видимости курсора в текущей позиции: 0 нет, 1 - есть
    void blinkDigit(uint8_t position);                              // задание позиции мигающей цифры
    void blinkDigits(uint8_t position, uint8_t length);             // задание диапазона мигающих цифр
    void blinkOff();                                                // выключить мигание цифр
    void setBlinkPeriod(uint16_t period);                           // установка периода мигания цифр и/или курсора (миллисекунды)
    void setOffset(uint8_t position);                               // задать текущую позицию для вывода

    void print(char value);                                         // вывод одного символа
    void print(const char* string);                                 // вывод строки
    void print(const __FlashStringHelper* string);                  // вывод строки из Flash
    void print(uint16_t value, uint8_t digits, uint8_t view = DEC); // вывод числа 16-битового
    void printSegments(uint8_t segments);                           // вывод произвольного символа посегментно (формат 0b'A-B-C-D-E-F-G-DP')
    void printDot(uint8_t position, uint8_t isShow = 1);            // управление состоянием точки индикатора


  private:

    enum            maskType_e { MASK_AND, MASK_OR, MASK_CLEAR };

    union {
      struct {
        unsigned    direction   : 1;
        unsigned    cursorShow  : 1;
        unsigned    blink       : 1;
        unsigned    isUpdating  : 1;
      };

      uint8_t       data;
    } _flag;

    union {
      struct {
        uint16_t    current;
        uint16_t    next;
        uint8_t     flags;
      };

      struct {
        uint8_t     current_n;
        uint8_t     current_r;
        uint8_t     next_n;
        uint8_t     next_r;

        unsigned    randMask    : 4;
        unsigned    rotation    : 1;
        unsigned    blink       : 1;
        unsigned    change      : 1;
        unsigned                : 1;
      };
    } _data[PANEL_DIGITS];

    uint8_t         _pin_clk;
    uint8_t         _pin_data;
    uint8_t         _pin_str;
    uint8_t         _pin_pwm;

    uint8_t         _brightness;
    uint8_t         _brightnessOld;
    uint8_t         _frameTime;
    uint32_t        _frameTimeNext;
    uint8_t         _frameCount;
    uint8_t         _position;
    uint16_t        _blinkPeriodHalf;
    uint8_t         _randSegment[BASIC_SEGMENTS_COUNT];

    panelEffect_t   _effect_type;
    panelEffect_t   _effect_type_next;


    void loadDigit(uint8_t index);
    void loadNextData();
    void loadData();
    void getDataChangeFlag();
    void maskData(uint8_t index, uint8_t index_of_char, maskType_e maskType);
    void getRandomBits();

    void selectEffect();
    void updateEffectFrame();

    void effectLight(uint8_t dir);          // эффекты LIGHT
    void effectCenter(uint8_t dir);         // эффекты CENTER
    void effectFlow(uint8_t dir);           // эффекты FLOW и WAVE
    void effectShift(uint8_t dir);          // эффекты SHIFT и MOVE
    void effectSegHideShow(uint8_t dir);    // эффекты HIDE и SHOW
    void effectSpin(uint8_t dir);           // эффекты SPIN
    void effectScroll(uint8_t dir);         // эффекты SCROLL

};


#endif /* SevenSegmentPanel_H */
