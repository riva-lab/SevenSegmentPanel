/*
  SevenSegmentPanel.h
  Created by RIVA, 2019.

  Драйвер панели на 7-сегментных индикаторах.
*/

#include <fastIO.h>
#include "SevenSegmentPanel.h"


/*******************************************************************************/

#ifdef FAST_IO_IMPLEMENTED

#define _pinMode        fastPinMode
#define _digitalWrite   fastDigitalWrite
#define _digitalRead    fastDigitalRead
#define _shiftOut       fastShiftOut

#else

#define _pinMode        pinMode
#define _digitalWrite   digitalWrite
#define _digitalRead    digitalRead
#define _shiftOut       shiftOut

#endif


#define APPLY_BRIGHTNESS(x) analogWrite(_pin_pwm, ~(x));


/*******************************************************************************/

SevenSegmentPanel::SevenSegmentPanel() { }

SevenSegmentPanel::SevenSegmentPanel(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm) {
  begin(pin_clk, pin_data, pin_str, pin_pwm);
}

SevenSegmentPanel::~SevenSegmentPanel() {
  end();
}

void SevenSegmentPanel::begin(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm) {
  _pinMode(_pin_data = pin_data, OUTPUT);
  _pinMode(_pin_clk  = pin_clk,  OUTPUT);
  _pinMode(_pin_str  = pin_str,  OUTPUT);
  _pinMode(_pin_pwm  = pin_pwm,  OUTPUT);

  for (uint8_t i = 0; i < PANEL_DIGITS; i++) {
    _data[i].current    = 0;
    _data[i].next       = 0;
    _data[i].flags      = 0;
  }

  APPLY_BRIGHTNESS(0);
  setFrameTime(SSP_DEFAULT_FRAME_TIME);
  setEffect(EFFECT_NONE);
  startUpdating();
  update();

  _flag.data = 0;
  setBrightness(SSP_DEFAULT_BRIGHTNESS);
  setBlinkPeriod(SSP_DEFAULT_BLINK_PERIOD);

  randomSeed(analogRead(0));
}

void SevenSegmentPanel::end() {
  _pinMode(_pin_data, INPUT);
  _pinMode(_pin_clk,  INPUT);
  _pinMode(_pin_str,  INPUT);
  _pinMode(_pin_pwm,  INPUT);
}

void SevenSegmentPanel::clear() {
  for (uint8_t i = 0; i < PANEL_DIGITS; i++)
    _data[i].next = 0;

  _position = 0;
}

void SevenSegmentPanel::update() {
  if (/*_flag.blink || _flag.cursorShow ||*/ ((int16_t)(millis() - _frameTimeNext) >= 0)) {
    _frameTimeNext += _frameTime;
    updateEffectFrame();

    APPLY_BRIGHTNESS(_brightness);

    _digitalWrite(_pin_str, 0);

    for (uint8_t i = 0; i < PANEL_DIGITS; i++)
      if (_flag.direction)  loadDigit(i);
      else                  loadDigit(PANEL_DIGITS - 1 - i);

    _digitalWrite(_pin_str, 1);
  }
}

void SevenSegmentPanel::autoUpdate() {
  if (isUpdating()) update();
}

void SevenSegmentPanel::startUpdating() {
  selectEffect();
  _frameTimeNext    = millis() - 1;
  _flag.isUpdating  = 1;
}

uint8_t SevenSegmentPanel::SevenSegmentPanel::isUpdating() {
  return _flag.isUpdating || _flag.cursorShow || _flag.blink;
}

uint8_t SevenSegmentPanel::digits() {
  return PANEL_DIGITS;
}

void SevenSegmentPanel::setDirection(uint8_t direction) {
  direction ? _flag.direction = 1 : _flag.direction = 0;
}

void SevenSegmentPanel::setDigitRotation(uint8_t position, uint8_t isRotate) {
  _data[position].rotation = (isRotate != 0);
}

void SevenSegmentPanel::setBrightness(uint8_t brightness) {
  _brightnessOld = _brightness = brightness;
  APPLY_BRIGHTNESS(_brightness);
}

void SevenSegmentPanel::setEffect(panelEffect_t effect_type) {
  _effect_type_next = _effect_type = effect_type;
}

void SevenSegmentPanel::setEffectNext(panelEffect_t effect_type) {
  _effect_type_next = effect_type;
}

void SevenSegmentPanel::setFrameTime(uint8_t milliseconds) {
  _frameTime = milliseconds;
}


void SevenSegmentPanel::cursorShow(uint8_t isShow) {
  _flag.cursorShow = (isShow != 0);
}

void SevenSegmentPanel::blinkDigit(uint8_t position) {
  if (position < PANEL_DIGITS) {
    _data[position].blink   = 1;
    _flag.blink             = 1;
  }
}

void SevenSegmentPanel::blinkDigits(uint8_t position, uint8_t length) {
  while (length--) blinkDigit(position++);
}

void SevenSegmentPanel::blinkOff() {
  _flag.blink = 0;
  for (uint8_t i = 0; i < PANEL_DIGITS; i++)
    _data[i].blink = 0;
}

void SevenSegmentPanel::setBlinkPeriod(uint16_t period) {
  _blinkPeriodHalf = period / 2;
}

void SevenSegmentPanel::setOffset(uint8_t position) {
  if (position <= PANEL_DIGITS) _position = position;
}


void SevenSegmentPanel::print(char value) {
  if      (_position >= PANEL_DIGITS)           return;
  if      ((value >= '0') && (value <= '9'))    value -= '0';
  else if ((value >= 'A') && (value <= 'Z'))    value -= 'A' - 10;
  else if ( value == '-')                       value  = 36;
  else if ( value == '*')                       value  = 37;
  else if ( value == '.')                       value  = 38;
  else if ( value == ' ')                       value  = BASIC_SYMBOLS_COUNT;
  else if ( value > BASIC_SYMBOLS_COUNT)        return;

  switch (value) {
    case 38: // точка выводится в текущее место
      if (--_position == 255) break;
      _data[_position].next_n |= font_n_char(38); // <.>
      _data[_position].next_r |= font_r_char(38); // <.>
      break;

    default: // остальные символы
      _data[_position].next = 0;

      if (value < BASIC_SYMBOLS_COUNT) {
        _data[_position].next_n = font_n_char(value);
        _data[_position].next_r = font_r_char(value);
        break;
      }
  }

  if ((value == BASIC_SYMBOLS_COUNT) ||
      (_data[_position].next_n != 0) )
    setOffset(_position + 1);
}

void SevenSegmentPanel::print(const char* string) {
  while (*string)
    if (_position > PANEL_DIGITS) return;
    else print(*string++);
}

void SevenSegmentPanel::print(const __FlashStringHelper* string) {
  const char* ptr = (const char*)(string);
  while (char x = pgm_read_byte(ptr++))
    if (_position > PANEL_DIGITS) return;
    else print(x);
}

void SevenSegmentPanel::print(uint16_t value, uint8_t digits, uint8_t view) {
  uint8_t data[5];
  uint8_t i = 0;

  do {
    if (value)  data[i] = value % 10, value /= 10; else data[i] = 0;
    if (digits) digits--;
    i++;
  } while ((value || digits) && (i < PANEL_DIGITS));

  while (i)
    print(char(data[--i]));
}

void SevenSegmentPanel::printSegments(uint8_t segments) {
  uint8_t mask          = 0x80;
  _data[_position].next = 0;

  for (uint8_t i = 0; i < 8; i++, mask >>= 1)
    if (segments & mask) {
      _data[_position].next_n |= font_n_char(BASIC_SEG_A + i);
      _data[_position].next_r |= font_r_char(BASIC_SEG_A + i);
    }

  if (_data[_position].next_n != 0)
    setOffset(++_position);
}

void SevenSegmentPanel::printDot(uint8_t position, uint8_t isShow) {
  if (position >= PANEL_DIGITS) return;

  if (isShow) {
    _data[position].next_n |=  font_n_char(BASIC_SEG_P);
    _data[position].next_r |=  font_r_char(BASIC_SEG_P);
  } else {
    _data[position].next_n &= ~font_n_char(BASIC_SEG_P);
    _data[position].next_r &= ~font_r_char(BASIC_SEG_P);
  }
}


void SevenSegmentPanel::loadDigit(uint8_t index) {

  uint8_t dn = _data[index].current_n;
  uint8_t dr = _data[index].current_r;
  uint16_t a;

  if (_flag.cursorShow || _data[index].blink)
    a = millis() % (_blinkPeriodHalf * 2);

  if (_flag.cursorShow && (index == _position) && (a > _blinkPeriodHalf)) {
    dn &= font_n_char(BASIC_SEG_P);
    dr &= font_r_char(BASIC_SEG_P);
    dn |= font_n_char(BASIC_SEG_D);
    dr |= font_r_char(BASIC_SEG_D);
  }

  if (_data[index].blink && (a > _blinkPeriodHalf)) {
    dn &= font_n_char(BASIC_SEG_P);
    dr &= font_r_char(BASIC_SEG_P);
  }

  _shiftOut(_pin_data, _pin_clk, LSBFIRST, _data[index].rotation ? dr : dn);
}

void SevenSegmentPanel::loadNextData() {
  for (uint8_t i = 0; i < PANEL_DIGITS; i++) {
    _data[i].current_n = _data[i].next_n;
    _data[i].current_r = _data[i].next_r;
  }
}

void SevenSegmentPanel::getDataChangeFlag() {
  for (uint8_t i = 0; i < PANEL_DIGITS; i++)
    _data[i].change = ((_data[i].current_n ^ _data[i].next_n) & ~font_n_char(BASIC_SEG_P)) > 0;
}

void SevenSegmentPanel::maskData(uint8_t index, uint8_t index_of_char, maskType_e maskType) {
  switch (maskType) {

    case MASK_AND:
      _data[index].current_n &=  font_n_char(index_of_char);
      _data[index].current_r &=  font_r_char(index_of_char);
      break;

    case MASK_OR:
      _data[index].current_n |=  font_n_char(index_of_char);
      _data[index].current_r |=  font_r_char(index_of_char);
      break;

    case MASK_CLEAR:
      _data[index].current_n &= ~font_n_char(index_of_char);
      _data[index].current_r &= ~font_r_char(index_of_char);
      break;
  }
}

void SevenSegmentPanel::getRandomBits() {
  uint8_t a;

  _randSegment[0] = random(8);
  for (uint8_t i = 1; i < 8; i++) {
    a = random(8);
    _data[i].randMask = random();

    uint8_t j = 0;
    do {
      if (a == _randSegment[j]) j = 0, a = random(8);
      else                      j++;
    } while (j < i);

    _randSegment[i] = a;
  }
}


void SevenSegmentPanel::selectEffect() {
  getDataChangeFlag();

  if (_effect_type != EFFECT_NONE) {
    _flag.cursorShow = 0;
    blinkOff();
  }

  switch (_effect_type) {
    case EFFECT_NONE:           _frameCount = 1;                    break;

    case EFFECT_LIGHT_FADE:
    case EFFECT_LIGHT_RISE:     _frameCount = _brightness / 4 + 4;  break;

    case EFFECT_LIGHT:          _frameCount = _brightness / 2 + 4;  break;

    case EFFECT_CENTER_IN:
    case EFFECT_CENTER_OUT:
    case EFFECT_CENTER:         _frameCount = ((PANEL_DIGITS + 1) / 2) * 2 * 3 + 1; break;

    case EFFECT_FLOW_LEFT:
    case EFFECT_FLOW_RIGHT:
    case EFFECT_WAVE_LEFT:
    case EFFECT_WAVE_RIGHT:     _frameCount = PANEL_DIGITS * 6; break;

    case EFFECT_SHIFT_LEFT:
    case EFFECT_SHIFT_RIGHT:    _frameCount = PANEL_DIGITS + 1; break;

    case EFFECT_MOVE_LEFT:
    case EFFECT_MOVE_RIGHT:     _frameCount = PANEL_DIGITS * 2; break;

    case EFFECT_SEG_HIDESHOW:
    case EFFECT_SEG_HIDE:
    case EFFECT_SEG_SHOW:       _frameCount = 19;   break;

    case EFFECT_SPIN_CW:
    case EFFECT_SPIN_CCW:       _frameCount = 9;    break;

    case EFFECT_SCROLL_DOWN:
    case EFFECT_SCROLL_UP:      _frameCount = 11;   break;
  }
}

void SevenSegmentPanel::updateEffectFrame() {
  if (_frameCount) {
    switch (_effect_type) {
      case EFFECT_NONE:         loadNextData();         break;
      case EFFECT_LIGHT_FADE:   effectLight(0);         break;
      case EFFECT_LIGHT_RISE:   effectLight(1);         break;
      case EFFECT_LIGHT:        effectLight(2);         break;
      case EFFECT_CENTER_IN:    effectCenter(0);        break;
      case EFFECT_CENTER_OUT:   effectCenter(1);        break;
      case EFFECT_CENTER:       effectCenter(2);        break;
      case EFFECT_FLOW_LEFT:    effectFlow(0);          break;
      case EFFECT_FLOW_RIGHT:   effectFlow(1);          break;
      case EFFECT_WAVE_LEFT:    effectFlow(2);          break;
      case EFFECT_WAVE_RIGHT:   effectFlow(3);          break;
      case EFFECT_SHIFT_LEFT:   effectShift(0);         break;
      case EFFECT_SHIFT_RIGHT:  effectShift(1);         break;
      case EFFECT_MOVE_LEFT:    effectShift(2);         break;
      case EFFECT_MOVE_RIGHT:   effectShift(3);         break;
      case EFFECT_SEG_HIDESHOW: effectSegHideShow(2);   break;
      case EFFECT_SEG_HIDE:     effectSegHideShow(0);   break;
      case EFFECT_SEG_SHOW:     effectSegHideShow(1);   break;
      case EFFECT_SPIN_CW:      effectSpin(0);          break;
      case EFFECT_SPIN_CCW:     effectSpin(1);          break;
      case EFFECT_SCROLL_DOWN:  effectScroll(1);        break;
      case EFFECT_SCROLL_UP:    effectScroll(0);        break;
    }
    _frameCount--;
  } else {
    _flag.isUpdating = 0;
    _effect_type     = _effect_type_next;
  }
}


void SevenSegmentPanel::effectLight(uint8_t dir) {
  if (_frameCount == 1) {
    _brightness = _brightnessOld;
    return;
  }

  if (_frameCount < 4) return;

  uint8_t step = _brightnessOld / 4 + 4;
  switch (dir) {
    case 0: step = _frameCount - 4;                                                 break;
    case 1: step = step - _frameCount;                                              break;
    case 2: step = (_frameCount > step) ? _frameCount - step : step - _frameCount;  break;
  }

  if ((_brightness = step * 4) == 0) loadNextData();
}

void SevenSegmentPanel::effectCenter(uint8_t dir) {
  uint8_t step  = _frameCount % 3;
  uint8_t i     = (((PANEL_DIGITS + 1) / 2) * 2 * 3 + 1 - _frameCount) / 3;
  uint8_t j     = PANEL_DIGITS - 1 - i;

  if (j == 255) {
    _frameCount = 1;
    return;
  }

  if ((i > (PANEL_DIGITS - 1) / 2) || ((i == j) && (step != 1))) {

    if (dir == 0) {
      _frameCount = 1;
      loadNextData();
      return;
    }

    _data[i].current = _data[i].next;
    _data[j].current = _data[j].next;

    switch (step) {
      case 0:
        maskData(i, 14, MASK_AND);
        maskData(j,  3, MASK_AND);
        break;

      case 1:
        maskData(i, 18, MASK_AND);
        maskData(j,  1, MASK_AND);
        break;
    }

  } else {

    if (dir == 1) {
      _frameCount = ((PANEL_DIGITS + 1) / 2) * 3 + 2 + (PANEL_DIGITS % 2) * 2;
      for (uint8_t i = 0; i < PANEL_DIGITS; i++) _data[i].current = 0;
      return;
    }

    switch (step) {
      case 0:
        maskData(i,  1, MASK_AND);
        maskData(j, 18, MASK_AND);
        break;

      case 1:
        maskData(i,  3, MASK_AND);
        maskData(j, 14, MASK_AND);
        break;

      default:
        _data[i].current = 0;
        _data[j].current = 0;
        break;
    }
  }
}

void SevenSegmentPanel::effectFlow(uint8_t dir) {
  uint8_t step  = _frameCount % 3;
  uint8_t i     = (PANEL_DIGITS * 6 - _frameCount) / 3 % PANEL_DIGITS;
  if (!(dir & 0x01)) i = PANEL_DIGITS - 1 - i;

  if (_frameCount > PANEL_DIGITS * 3) {
    switch (step) {
      case 0:  maskData(i, (dir & 0x01) ? 3 : 14, MASK_AND); break;
      case 2:  maskData(i, (dir & 0x01) ? 1 : 18, MASK_AND); break;
      default: _data[i].current = 0;                         break;
    }
  } else {
    if (!(dir & 0x02)) {
      loadNextData();
      _frameCount = 1;
      return;
    }

    _data[i].current = _data[i].next;
    switch (step) {
      case 0:  maskData(i, (dir & 0x01) ? 18 : 1, MASK_AND); break;
      case 2:  maskData(i, (dir & 0x01) ? 14 : 3, MASK_AND); break;
    }
  }
}

void SevenSegmentPanel::effectShift(uint8_t dir) {
  if ((dir & 0x02) && (_frameCount <= PANEL_DIGITS)) {

    if (dir & 0x01)
      for (uint8_t i = PANEL_DIGITS - _frameCount, j = PANEL_DIGITS - 1; i != 255; i--, j--)
        _data[i].current = _data[j].next;
    else
      for (uint8_t i = _frameCount - 1, j = 0; i < PANEL_DIGITS; i++, j++)
        _data[i].current = _data[j].next;

  } else {

    if (dir & 0x01) {
      for (uint8_t i = PANEL_DIGITS - 1; i > 0; i--)
        _data[i].current = _data[i - 1].current & ~font_n_char(BASIC_SEG_P);
      _data[0].current = 0;
    } else {
      for (uint8_t i = 0; i < PANEL_DIGITS - 1; i++)
        _data[i].current = _data[i + 1].current & ~font_n_char(BASIC_SEG_P);
      _data[PANEL_DIGITS - 1].current = 0;
    }
  }

  if (_frameCount == 1) loadNextData();
}

void SevenSegmentPanel::effectSegHideShow(uint8_t dir) {
  for (uint8_t i = 0; i < PANEL_DIGITS; i++) {
    switch (_frameCount) {

      case 19:
        if (dir == 1) _frameCount = 11;
        getRandomBits();
        return;

      case 10:
        if (i   == 0) getRandomBits();
        if (dir == 0) _frameCount = 1;
        _data[i].current = 0;
        break;

      case 1:
        loadNextData();
        return;

      default:
        uint8_t ending = _frameCount < 10;
        uint8_t index  = ending ? _frameCount - 2 : _frameCount - 11;
        uint8_t seg    = BASIC_SYMBOLS_COUNT - BASIC_SEGMENTS_COUNT + (_randSegment[index] ^ _data[i].randMask) & 0x07;
        maskData(i, seg, ending ? MASK_OR : MASK_CLEAR);
        if (ending) _data[i].current &= _data[i].next;
        break;
    }
  }
}

void SevenSegmentPanel::effectSpin(uint8_t dir) {
  for (uint8_t i = 0; i < PANEL_DIGITS; i++)
    if (_data[i].change)
      if (_data[i].next_n == 0) _data[i].current = 0;
      else {
        uint8_t seg = 0;

        switch (_frameCount) {
          case 9:  _data[i].current = 0;                                  break;
          case 2:
          case 8:  !(dir & 0x01) ? seg = BASIC_SEG_D : seg = BASIC_SEG_A; break;
          case 7:  !(dir & 0x01) ? seg = BASIC_SEG_E : seg = BASIC_SEG_F; break;
          case 6:  !(dir & 0x01) ? seg = BASIC_SEG_F : seg = BASIC_SEG_E; break;
          case 5:  !(dir & 0x01) ? seg = BASIC_SEG_A : seg = BASIC_SEG_D; break;
          case 4:  !(dir & 0x01) ? seg = BASIC_SEG_B : seg = BASIC_SEG_C; break;
          case 3:  !(dir & 0x01) ? seg = BASIC_SEG_C : seg = BASIC_SEG_B; break;
          case 1:  loadNextData();                                        break;
        }

        if (seg) {
          _data[i].current_n = font_n_char(seg);
          _data[i].current_r = font_r_char(seg);
        }
      }
}

void SevenSegmentPanel::effectScroll(uint8_t dir) {
  if (_frameCount < 6) loadNextData();

  for (uint8_t i = 0; i < PANEL_DIGITS; i++)
    if (_data[i].change) {
      switch (_frameCount) {

        case 11:
          maskData(i, dir ? BASIC_SEG_A : BASIC_SEG_D, MASK_CLEAR);
          break;

        case 10:
          maskData(i, dir ? BASIC_SEG_B : BASIC_SEG_C, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_F : BASIC_SEG_E, MASK_CLEAR);
          break;

        case 9:
          maskData(i, BASIC_SEG_G, MASK_CLEAR);
          break;

        case 8:
          maskData(i, dir ? BASIC_SEG_C : BASIC_SEG_B, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_E : BASIC_SEG_F, MASK_CLEAR);
          break;

        case 7:
          maskData(i, dir ? BASIC_SEG_D : BASIC_SEG_A, MASK_CLEAR);
          break;

        case 5:
          uint8_t dp = _data[i].current_n & font_n_char(BASIC_SEG_P);
          maskData(i, dir ? BASIC_SEG_A : BASIC_SEG_D, MASK_AND);
          if (dp) maskData(i, BASIC_SEG_P, MASK_OR);
          break;

        case 4:
          maskData(i, dir ? BASIC_SEG_D : BASIC_SEG_A, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_C : BASIC_SEG_B, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_E : BASIC_SEG_F, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_G : BASIC_SEG_G, MASK_CLEAR);
          break;

        case 3:
          maskData(i, dir ? BASIC_SEG_D : BASIC_SEG_A, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_C : BASIC_SEG_B, MASK_CLEAR);
          maskData(i, dir ? BASIC_SEG_E : BASIC_SEG_F, MASK_CLEAR);
          break;

        case 2:
          maskData(i, dir ? BASIC_SEG_D : BASIC_SEG_A, MASK_CLEAR);
          break;

        case 1:
          break;
      }
    }
}
