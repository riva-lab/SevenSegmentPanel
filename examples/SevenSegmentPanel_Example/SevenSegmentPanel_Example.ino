/*
  Example for library SevenSegmentPanel.h
  Created by RIVA, 2019.

  Пример работы с драйвером панели на 7-сегментных индикаторах.

  Индикаторы подключены к сдвиговым регистрам HC595 с использованием ULN2003
  (или напрямую, если потребляемый индикатором ток не превышает допустимого).
  Регистры соединяются последовательно.

  Панель управляется по 4 линиям:
  1 CLK     тактовый сигнал для регистров
  2 DATA    данные, загружаемые в регистры
  3 STR     сигнал разрешения обновления данных
  4 PWM     ШИМ-сигнал управления яркостью панели

  При указании выводов подключения панели для линии PWM следует использовать
  тот вывод, на котором доступен аппаратный ШИМ(~).

  Количество индикаторов нужно задать макросом PANEL_DIGITS, отредактировав
  его в файле "SevenSegmentPanel.h" (строка 42).
  По умолчанию - 8.

  Все функции, изменяющие отображаемые данные, работают во внутреннем буфере.
  Чтобы увидеть реальное изменение данных, необходимо использовать
  метод update() или autoUpdate().
*/

#include "SevenSegmentPanel.h"


#define PIN_CLK         8
#define PIN_DATA        5
#define PIN_STR         7
#define PIN_PWM         6

#define TIME_S          4


SevenSegmentPanel ssp(PIN_CLK, PIN_DATA, PIN_STR, PIN_PWM);
//SevenSegmentPanel ssp;


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(115200);
  Serial.println(F("start"));

  // инициализируем панель и задаем важные параметры

  //  ssp.begin(PIN_CLK, PIN_DATA, PIN_STR, PIN_PWM);

  ssp.setDirection(1);          // задаем направление вывода данных в регистры
  ssp.setDigitRotation(2, 1);   // 2-й индикатор перевернут (отсчет от 0)
  ssp.setDigitRotation(4, 1);   // 4-й индикатор перевернут (отсчет от 0)
  ssp.setBrightness(100);       // устанавливаем яркость панели
}


void loop() {

  static uint32_t   tx  = 0;
  uint32_t          t   = (millis() / 1000);
  uint8_t           q   = 0;
  if (t > tx)       tx = t, q = (millis() % 1000) < 10;

  //  if (q)
  //        Serial.println(q);
        
  if (q)
    switch (t) {

      case 4:
        Serial.println(F("print(char), print(string), print(int)"));

        ssp.clear();
        ssp.print('A');
        ssp.print("B.C.");
        ssp.setOffset(4);
        ssp.print(123, 0);
        ssp.startUpdating();
        break;

      case 6:
        Serial.println(F("clear()"));

        ssp.clear();
        ssp.startUpdating();
        break;

      case 8:
        Serial.println(F("printSegments(ABCDEFGP), setOffset(), printDot(), blinkDigit()"));

        ssp.clear();
        ssp.print(F("F"));
        ssp.printSegments(0b00010010); // '='
        ssp.setOffset(ssp.digits() - 5);
        ssp.print(12345, 0);
        ssp.printDot(3);
        ssp.printDot(4);
        ssp.startUpdating();

        ssp.setBlinkPeriod(700);
        ssp.blinkDigit(0);
        ssp.blinkDigit(2);
        break;

      case 12:
        Serial.println(F("blinkOff()"));

        ssp.printDot(4, 0);
        ssp.startUpdating();

        ssp.blinkOff();
        break;

      case 14:
        Serial.println(F("cursorShow(true)"));

        ssp.clear();
        ssp.print(123, 4);
        ssp.setOffset(6);
        ssp.print(4);
        ssp.setOffset(4);
        ssp.cursorShow(1);
        ssp.startUpdating();
        break;

      case 16:
        Serial.println(F("cursorShow(false)"));

        ssp.cursorShow(0);
        ssp.startUpdating();
        break;

      case 18:
        Serial.println(F("demo of too long string"));

        ssp.clear();
        ssp.print(F("1--4--7--string-long"));
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 0:
        Serial.println(F("effect EFFECT_LIGHT_FADE"));

        ssp.clear();
        ssp.print(F(" FADE"));
        ssp.setFrameTime(30);
        ssp.setEffect(EFFECT_LIGHT_FADE);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 1:
        Serial.println(F("effect EFFECT_LIGHT_RISE"));

        ssp.clear();
        ssp.print(F(" RISE"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_LIGHT_RISE);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 2:
        Serial.println(F("effect EFFECT_LIGHT"));

        ssp.clear();
        ssp.print(F(" LIGHT"));
        ssp.setFrameTime(20);
        ssp.setEffect(EFFECT_LIGHT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 3:
        Serial.println(F("effect EFFECT_CENTER_IN"));

        ssp.clear();
        ssp.print(F("CTR--IN"));
        ssp.setFrameTime(50);
        ssp.setEffect(EFFECT_CENTER_IN);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 4:
        Serial.println(F("effect EFFECT_CENTER_OUT"));

        ssp.clear();
        ssp.print(F("CTR-OUT"));
        ssp.setFrameTime(50);
        ssp.setEffect(EFFECT_CENTER_OUT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 5:
        Serial.println(F("effect EFFECT_CENTER"));

        ssp.clear();
        ssp.print(F("CENTER"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_CENTER);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 6:
        Serial.println(F("effect EFFECT_FLOW_LEFT"));

        ssp.clear();
        ssp.print(F("FL-LEFT"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_FLOW_LEFT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 7:
        Serial.println(F("effect EFFECT_FLOW_RIGHT"));

        ssp.clear();
        ssp.print(F("FLRIGHT"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_FLOW_RIGHT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 8:
        Serial.println(F("effect EFFECT_WAVE_LEFT"));

        ssp.clear();
        ssp.print(F("UE-LEFT"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_WAVE_LEFT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 9:
        Serial.println(F("effect EFFECT_WAVE_RIGHT"));

        ssp.clear();
        ssp.print(F("UE-RGHT"));
        ssp.setFrameTime(40);
        ssp.setEffect(EFFECT_WAVE_RIGHT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 10:
        Serial.println(F("effect EFFECT_SHIFT_LEFT"));

        ssp.clear();
        ssp.print(F("SHIFT-L"));
        ssp.setFrameTime(100);
        ssp.setEffect(EFFECT_SHIFT_LEFT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 11:
        Serial.println(F("effect EFFECT_SHIFT_RIGHT"));

        ssp.clear();
        ssp.print(F("SHIFT-R"));
        ssp.setFrameTime(100);
        ssp.setEffect(EFFECT_SHIFT_RIGHT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 12:
        Serial.println(F("effect EFFECT_MOVE_LEFT"));

        ssp.clear();
        ssp.print(F("O--LEFT"));
        ssp.setFrameTime(50);
        ssp.setEffect(EFFECT_MOVE_LEFT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 13:
        Serial.println(F("effect EFFECT_MOVE_RIGHT"));

        ssp.clear();
        ssp.print(F("O-RIGHT"));
        ssp.setFrameTime(50);
        ssp.setEffect(EFFECT_MOVE_RIGHT);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 14:
        Serial.println(F("effect EFFECT_SEG_HIDE"));

        ssp.clear();
        ssp.print(F("SEG-HD"));
        ssp.setFrameTime(150);
        ssp.setEffect(EFFECT_SEG_HIDE);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 15:
        Serial.println(F("effect EFFECT_SEG_SHOW"));

        ssp.clear();
        ssp.print(F("SEG--SH"));
        ssp.setFrameTime(150);
        ssp.setEffect(EFFECT_SEG_SHOW);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 16:
        Serial.println(F("effect EFFECT_SEG_HIDESHOW"));

        ssp.clear();
        ssp.print(F("SEG--HS"));
        ssp.setFrameTime(120);
        ssp.setEffect(EFFECT_SEG_HIDESHOW);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 17:
        Serial.println(F("effect EFFECT_SPIN_CW"));

        ssp.clear();
        ssp.print(F("-SPIN-C"));
        ssp.setFrameTime(60);
        ssp.setEffect(EFFECT_SPIN_CW);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 18:
        Serial.println(F("effect EFFECT_SPIN_CCW"));

        ssp.clear();
        ssp.print(F("SPIN-CC"));
        ssp.setFrameTime(60);
        ssp.setEffect(EFFECT_SPIN_CCW);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 19:
        Serial.println(F("effect EFFECT_SCROLL_DOWN"));

        ssp.clear();
        ssp.print(F("SCRL-DN"));
        ssp.setFrameTime(60);
        ssp.setEffect(EFFECT_SCROLL_DOWN);
        ssp.startUpdating();
        break;

      case 20 + TIME_S * 20:
        Serial.println(F("effect EFFECT_SCROLL_UP"));

        ssp.clear();
        ssp.print(F("-SCRLUP"));
        ssp.setFrameTime(60);
        ssp.setEffect(EFFECT_SCROLL_UP);
        ssp.startUpdating();
        break;
    }


  // обновляем данные на панели по необходимости
  if (ssp.isUpdating()) {
    digitalWrite(LED_BUILTIN, 1);

    ssp.update();

    digitalWrite(LED_BUILTIN, 0);
  }

  //  // или можно воспользоваться этим методом вместо обновления по условию
  //  ssp.autoUpdate();

}
