# SevenSegmentPanel

Драйвер панели на 7-сегментных индикаторах, библиотека Arduino.

Панель составляется из 7-сегментных индикаторов, которые подключены к сдвиговым регистрам HC595 с использованием ULN2003 (или напрямую, если потребляемый индикатором ток не превышает допустимого). Регистры соединяются последовательно.

Панель управляется по 4 линиям:

| #    | Линия | Описание                              |
| ---- | ----- | ------------------------------------- |
| 1    | CLK   | Тактовый сигнал для регистров         |
| 2    | DATA  | Данные, загружаемые в регистры        |
| 3    | STR   | Сигнал разрешения обновления данных   |
| 4    | PWM   | ШИМ-сигнал управления яркостью панели |

Для линии PWM следует использовать тот вывод, на котором доступен аппаратный ШИМ.

Количество индикаторов нужно задать макросом `PANEL_DIGITS`, отредактировав его в файле `SevenSegmentPanel.h` (строка 46). По умолчанию — 8.





## Установка

### Используя Arduino IDE Library Manager

1. Откройте Менеджер библиотек `Скетч > Подключить библиотеку > Управлять библиотеками...` (`Ctrl+Shift+I`).
2. Введите `SevenSegmentPanel` в поле поиска.
3. Выберите строку с библиотекой.
4. Выберите версию и щелкните кнопку `Установка`.

### Используя Git

```sh
cd ~/Documents/Arduino/libraries/
git clone https://gitlab.com/riva-lab/SevenSegmentPanel SevenSegmentPanel
```





## Программный интерфейс

##### Создание экземпляра

```c++
SevenSegmentPanel();
SevenSegmentPanel(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm);
```



##### Уничтожение экземпляра

```c++
~SevenSegmentPanel();
void end();
```

> Освобождает подключенные пины, настраивая их на вход.



##### Инициализация

```
void begin(uint8_t pin_clk, uint8_t pin_data, uint8_t pin_str, uint8_t pin_pwm);
```

> Используется для переинициализации или для начальной инициализации с конструктором `SevenSegmentPanel()`.



##### Управление

```c++
void    clear();            // очистка панели
void    update();           // обновление информации на индикаторах (реальное)
void    autoUpdate();       // обновление информации на индикаторах (реальное)
void    startUpdating();    // начать обновление с применением заданного эффекта
uint8_t isUpdating();       // флаг обновления: 1 - идет, 0 - завершено
uint8_t digits();           // количество знакомест (цифр) панели
```

> Все функции, изменяющие отображаемые данные, работают во внутреннем буфере. Чтобы обновить отображение на панели, необходимо использовать метод `update()` или `autoUpdate()`.



##### Настройки

```c++
void setDirection(uint8_t direction);                           // направление вывода цифр на панели
void setDigitRotation(uint8_t position, uint8_t isRotate);      // установка поворота цифры
void setBrightness(uint8_t brightness);                         // установка яркости панели (0 - выкл, 255 - максимальная)
void setEffect(panelEffect_t effect_type = EFFECT_NONE);        // установка эффекта для обновлений панели
void setEffectNext(panelEffect_t effect_type = EFFECT_NONE);    // установка эффекта для следующих обновлений панели
void setFrameTime(uint8_t milliseconds);                        // установка длительности 1 кадра эффекта (миллисекунды)
```



##### Управление курсором

```c++
void cursorShow(uint8_t isShow);    // задание видимости курсора в текущей позиции: 0 нет, 1 - есть
void setOffset(uint8_t position);   // задать текущую позицию для вывода
```



##### Управление миганием

```c++
void blinkDigit(uint8_t position);                  // задание позиции мигающей цифры
void blinkDigits(uint8_t position, uint8_t length); // задание диапазона мигающих цифр
void blinkOff();                                    // выключить мигание цифр
void setBlinkPeriod(uint16_t period);               // установка периода мигания цифр и/или курсора (миллисекунды)
```



##### Вывод данных

```c++
void print(char value);                                         // вывод одного символа
void print(const char* string);                                 // вывод строки
void print(const __FlashStringHelper* string);                  // вывод строки из Flash
void print(uint16_t value, uint8_t digits, uint8_t view = DEC); // вывод числа 16-битового
void printSegments(uint8_t segments);                           // вывод произвольного символа посегментно
void printDot(uint8_t position, uint8_t isShow = 1);            // управление состоянием точки индикатора
```

> Вывод произвольного символа посегментно происходит в формате `0b A-B-C-D-E-F-G-DP`.



##### Тип данных: `panelEffect_t` — эффект смены данных

```c++
enum panelEffect_e {
  EFFECT_NONE,              // нет эффекта смены
  EFFECT_LIGHT_FADE,        // плавное затухание всей панели
  EFFECT_LIGHT_RISE,        // плавное зажигание всей панели
  EFFECT_LIGHT,             // плавное затухание/зажигание всей панели
  EFFECT_CENTER_IN,         // схлопывание к центру
  EFFECT_CENTER_OUT,        // развертывание от центра
  EFFECT_CENTER,            // комбинация схлопывание + развертывание
  EFFECT_FLOW_LEFT,         // волнообразное исчезновение влево
  EFFECT_FLOW_RIGHT,        // волнообразное исчезновение вправо
  EFFECT_WAVE_LEFT,         // волна влево
  EFFECT_WAVE_RIGHT,        // волна вправо
  EFFECT_SHIFT_LEFT,        // сдвиг текущих данных влево
  EFFECT_SHIFT_RIGHT,       // сдвиг текущих данных вправо
  EFFECT_MOVE_LEFT,         // передвижение текущих данных влево с заменой новыми
  EFFECT_MOVE_RIGHT,        // передвижение текущих данных вправо с заменой новыми
  EFFECT_SEG_HIDE,          // посегментное исчезновение
  EFFECT_SEG_SHOW,          // посегментное появление
  EFFECT_SEG_HIDESHOW,      // посегментное исчезновение/появление
  EFFECT_SPIN_CW,           // вращение меняющихся данных по ч.с.
  EFFECT_SPIN_CCW,          // вращение меняющихся данных против ч.с.
  EFFECT_SCROLL_DOWN,       // перелистывание меняющихся данных вниз
  EFFECT_SCROLL_UP          // перелистывание меняющихся данных вверх
};
```





## Примеры

##### Простейший пример

```c++
#include "SevenSegmentPanel.h"

SevenSegmentPanel ssp(8, 5, 7, 6); // PIN_CLK, PIN_DATA, PIN_STR, PIN_PWM

void setup() {                  // инициализируем панель и задаем важные параметры:
  ssp.setDirection(1);          //  - задаем направление вывода данных в регистры
  ssp.setBrightness(100);       //  - устанавливаем яркость панели
  ssp.clear();                  // очистка панели
  ssp.print(F("123-"));         // вывод строки из Flash
  ssp.print(45);                // вывод числа
  ssp.startUpdating();          // начать обновление
}

void loop() {
  ssp.autoUpdate();             // обновляем данные на панели по необходимости
}
```

Другие примеры смотрите в каталоге [examples](examples).





## Лицензия

Библиотека распространяется под [лицензией](license) [MIT](https://opensource.org/licenses/mit-license.php).



